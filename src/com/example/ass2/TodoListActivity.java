package com.example.ass2;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;

/**
 * Activity that displays a list of todo elements for one todo category. Todo list is interactable.
 * 
 * @author Yiming Yan, Evgueni Freiman
 * @version 2015-11-02
 */
public class TodoListActivity extends Activity {
	public static final int SHOW_AS_ACTION_IF_ROOM = 1;
	public static final String CATEGORYKEY="category";
	public static final String IDKEY="id";
	public static final String CONTENTKEY="content";
	private ListView lv;
	private TextView tv;
	private SimpleCursorAdapter sca;
	private static DBHelper dbh;
	private Cursor cursor;
	private String category;
	private SharedPreferences prefs;
	private long lastId;
	private Menu m;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		String[] from = { DBHelper.COLUMN_CONTENT };
		// matching fields on the layout to be used with the adapter
		int[] to = { R.id.contentrow_textview };
		/*
		 * setContentView(): inflate the main layout (UI) from activity_main.xml
		 * inflation: xml -> ojbect(s)
		 */
		setContentView(R.layout.activity_showtodo);

		// instantiate the DBHelper class
		dbh = DBHelper.getDBHelper(this);

		category = getIntent().getStringExtra(CATEGORYKEY);

		cursor = dbh.getTodoContentByCategory(category);


		// the objects are now created, get a reference to the listview object
		lv = (ListView) findViewById(R.id.showtodo_listview);
		tv = (TextView) findViewById(R.id.showtodo_textview);
		tv.setText(category);

		sca = new SimpleCursorAdapter(this, R.layout.content_row, cursor, from, to, 0);

		lv.setAdapter(sca);

		/*
		 * Once the adapter is assigned you may want an item click to be an
		 * event that it responded to so you must set up a listener for the
		 * ListView
		 */
		lv.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

				final int delete = (int) id;

				AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(view.getContext());

				// set dialog message
				alertDialogBuilder.setMessage(R.string.msg).setCancelable(false)
						.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						// if this button is clicked, close
						// current activity
						dbh.deleteToDo(delete);
						refreshView();
					}
				}).setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						// if this button is clicked, just close
						// the dialog box and do nothing
						dialog.cancel();
					}
				});

				// create alert dialog
				AlertDialog alertDialog = alertDialogBuilder.create();

				// show it
				alertDialog.show();

			}
		});

		lv.setOnItemLongClickListener(new OnItemLongClickListener() {
			public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {

				Toast.makeText(getApplicationContext(), ((TextView) view).getText(), Toast.LENGTH_SHORT).show();

				Intent i = new Intent(view.getContext(), TodoEditorActivity.class);

				i.putExtra(IDKEY, id);
				i.putExtra(CATEGORYKEY, category);
				i.putExtra(CONTENTKEY, ((TextView) view).getText());
				startActivity(i);

				return true;
			}
		});
	}
	
	@Override
	public void onResume() {
		super.onResume();

		// Get the lastId from sharedPrefs. If it is set, display the "Last" menu option.
		prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
		lastId = prefs.getLong(DBHelper.COLUMN_ID, -1);
		if (m!=null&&lastId!=-1){
			m.getItem(1).setVisible(true);
		}
		refreshView();
	}

	@Override
	public void onPause() {
		super.onPause();
		// release resources
		dbh.close();
	}

	// Updates the todo data displayed in the ui.
	private void refreshView() {
		// renew the cursor
		cursor = dbh.getTodoContentByCategory(category);
		// have the adapter use the new cursor, changeCursor closes old cursor
		// too
		sca.changeCursor(cursor);
		// have the adapter tell the observers
		sca.notifyDataSetChanged();

	}

	/**
	 *  Called by the Add New button's onclick event. Opens the TodoEditor activity.
	 * @param view
	 */
	public void addToDoItem(View view) {
		Intent i = new Intent(view.getContext(), TodoEditorActivity.class);
		i.putExtra(CATEGORYKEY, category);
		startActivity(i);
		refreshView();

	}

	/**
	 * Create our menu items. "Last" menu item only appears when there is a last todo.
	 */
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		// Set up the intents for each button
		Intent today = new Intent(this, TodayTodoActivity.class);
		Intent last = new Intent(this, LastTodoActivity.class);
		logIt("todolistactivity onCreateOptionsMenu: " + lastId);
		menu.add(R.string.today).setIntent(today).setShowAsAction(SHOW_AS_ACTION_IF_ROOM);
		menu.add(R.string.last).setIntent(last).setShowAsAction(SHOW_AS_ACTION_IF_ROOM);
		
		if (lastId == -1) {
			menu.getItem(1).setVisible(false);
		}
		
		m=menu;
		
		return true;
	}

	private void logIt(String msg) {
		final String TAG = "ass2";
		Log.d(TAG, msg);
	}
}
