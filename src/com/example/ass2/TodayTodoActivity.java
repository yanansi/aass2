package com.example.ass2;

import android.app.Activity;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.widget.TextView;

/**
 * Activity that displays today's todo item
 * 
 * @author Yiming Yan
 * @version 2015-11-02
 */
public class TodayTodoActivity extends Activity {
	public static final int SHOW_AS_ACTION_IF_ROOM = 1;
	private static DBHelper dbh;
	private long lastId;
	private SharedPreferences prefs;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.today);
		
		TextView category = (TextView) findViewById(R.id.today_textview1);
		TextView content = (TextView) findViewById(R.id.today_textview2);
		
		// instantiate the DBHelper class
		dbh = DBHelper.getDBHelper(this);
		
		// Get a random todo item from the db, then display show it in the ui.
		Cursor cursor = dbh.getTodoContentForToday();

		if (cursor.getCount()!=0){
		cursor.moveToFirst();
		category.setText(cursor.getString(cursor.getColumnIndex(DBHelper.COLUMN_CATEGORY)));
		content.setText(cursor.getString(cursor.getColumnIndex(DBHelper.COLUMN_CONTENT)));
		lastId = cursor.getLong(cursor.getColumnIndex(DBHelper.COLUMN_ID));}
		else{
			content.setText(R.string.emtpydb);
		}
	}

	@Override
	public void onPause() {
		prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
		SharedPreferences.Editor editor = prefs.edit();
		// set the key/value pairs
		editor.putLong(DBHelper.COLUMN_ID, lastId);

		// don't forget to commit the changes
		editor.commit();
		super.onPause();
	}
}
