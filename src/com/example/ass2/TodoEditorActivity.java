package com.example.ass2;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

/**
 * Activity that displays one todo item that can be edited.
 * 
 * @author Yiming Yan, Evgueni Freiman
 * @version 2015-11-02
 */
public class TodoEditorActivity extends Activity {
	private TextView category;
	private EditText content;
	private static DBHelper dbh;
	private Long id = (long) -1;
	private SharedPreferences prefs;
	private String categoryStr;
	private long lastId;
	private boolean newTodo;
	public static final String CATEGORYKEY="category";
	public static final String IDKEY="id";
	public static final String CONTENTKEY="content";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_todoeditor);
		category = (TextView) findViewById(R.id.editor_textview1);
		content = (EditText) findViewById(R.id.editor_edittext2);
		
		// If idkey exists, this is a todo item being edited
		if (getIntent().hasExtra(IDKEY)) {
			// Retrieve todo data from intent
			id = getIntent().getLongExtra(IDKEY, -1);
			lastId = id;
			saveLastTodo(id);
			category.setText(getIntent().getStringExtra(CATEGORYKEY));
			content.setText(getIntent().getStringExtra(CONTENTKEY));
			newTodo = false;
		} else {
		// if idkey does not exist this is a new todo item being added
			categoryStr = getIntent().getStringExtra(CATEGORYKEY);
			newTodo = true;
		}
		// instantiate the DBHelper class
		dbh = DBHelper.getDBHelper(this);
	}

	/**
	 * Called by the Save button's onclick, saves the changes to the todo item.
	 * @param view
	 */
	public void editTodo(View view) {
		String cate = category.getText().toString();
		String con = content.getText().toString().trim();
		if (newTodo&&!con.isEmpty()) {
			lastId = dbh.insertNewToDo(categoryStr, con);
		} else {
			
			dbh.updateTodo(id, cate, con);
		}
		
		saveLastTodo(lastId);
		finish();
	}

	// Saves the last seen todo id to sharedPrefs. To be retrieved by the LastTodoActivity.
	private void saveLastTodo(long id) {
		prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
		SharedPreferences.Editor editor = prefs.edit();

		// set the key/value pairs
		logIt("Editor saved id " + lastId);
		editor.putLong(DBHelper.COLUMN_ID, lastId);
		editor.commit();
	}

	private void logIt(String msg) {
		final String TAG = "ass2";
		Log.d(TAG, msg);
	}
}
