package com.example.ass2;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * DBHelper.class
 * is an SQLiteOpenHelper class
 * It is a simple implementation, if you use it use it as a base only you must add a lot of functionality
 * It creates a database and implements some of the CRUD methods
 */
public class DBHelper extends SQLiteOpenHelper {

	// table name
	public static final String TABLE_TODOLIST = "todolist";
	// database field names (COLUMN_
	public static final String COLUMN_ID = "_id";
	public static final String COLUMN_CATEGORY = "category";
	public static final String COLUMN_CONTENT = "content";
	// file name
	private static final String DATABASE_NAME = "todolist.db";
	// if the version number is increased the onUpdate() will be called
	private static final int DATABASE_VERSION = 1;

	// Database creation raw SQL statement
	private static final String DATABASE_CREATE = "create table " + TABLE_TODOLIST + "(" + COLUMN_ID
			+ " integer primary key autoincrement, " + COLUMN_CATEGORY + " text not null, " + COLUMN_CONTENT
			+ " text not null" + ");";

	// static instance to share DBHelper
	private static DBHelper dbh = null;

	public DBHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
		// TODO Auto-generated constructor stub
	}

	/**
	 * getDBHelper(Context)
	 * 
	 * The static factory method getDBHelper make's sure that only one database
	 * helper exists across the app's lifecycle
	 * 
	 * A factory is an object for creating other objects. It is an abstraction
	 * of a constructor.
	 */
	public static DBHelper getDBHelper(Context context) {

		if (dbh == null) {
			dbh = new DBHelper(context.getApplicationContext());
		}
		return dbh;
	}

	/**
	 * onCreate()
	 * 
	 * SQLiteOpenHelper lifecycle method used when we first create the database,
	 * in this case we create an empty database. You may want to populate your
	 * database with data when you create it, this is app dependent.
	 * 
	 * @see android.database.sqlite.SQLiteOpenHelper#onCreate(android.database.sqlite
	 *      .SQLiteDatabase)
	 */
	@Override
	public void onCreate(SQLiteDatabase database) {
		logIt("in onCreate");
		createPopulateDB(database);

	}

	/**
	 * onUpgrade()
	 * 
	 * SQLiteOpenHelper lifecycle method used when the database is upgraded to a
	 * different version, this one is simple, it drops then recreates the
	 * database, you loose all of the data This is not necessarily what you will
	 * want to do :p
	 * 
	 * @see android.database.sqlite.SQLiteOpenHelper#onUpgrade(android.database.sqlite
	 *      .SQLiteDatabase, int, int)
	 */
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		Log.w(DBHelper.class.getName(), "Upgrading database from version " + oldVersion + " to " + newVersion
				+ ", which will destroy all old data");
		try {
			db.execSQL("DROP TABLE IF EXISTS " + TABLE_TODOLIST);
		} catch (SQLException e) {
			Log.e(DBHelper.class.getName(), "DROP exception" + Log.getStackTraceString(e));
			throw e;
		}
		onCreate(db);
	}

	/**
	 * onOpen()
	 * 
	 * SQLiteOpenHelper lifecycle method called when we open the database,
	 * 
	 * @param SQLiteDatabase
	 * @see android.database.sqlite.SQLiteOpenHelper#onOpen(android.database.sqlite.SQLiteDatabase)
	 *      .SQLiteDatabase)
	 */
	@Override
	public void onOpen(SQLiteDatabase database) {
		Log.i(DBHelper.class.getName(), "onOpen()");
	}

	public void createPopulateDB(SQLiteDatabase database) {

		try {
			database.execSQL(DATABASE_CREATE);
		} catch (SQLException e) {
			Log.e(DBHelper.class.getName(), "CREATE exception" + Log.getStackTraceString(e));
			throw e;
		}

		String sql = "INSERT INTO " + TABLE_TODOLIST + " VALUES(null, 'School', 'Go to school')";
		try {
			database.execSQL(sql);
		} catch (SQLException e) {
			Log.e(DBHelper.class.getName(), "INSERT exception" + Log.getStackTraceString(e));
			throw e;
		}
	}

	/**
	 * insertNewTodo()
	 * 
	 * CR*U*D Update
	 * 
	 * @param category
	 * @param content
	 * @return the return code from the insert
	 *
	 *         Using ContentValues insert a record in the database
	 */
	public long insertNewToDo(String category, String content) {
		ContentValues cv = new ContentValues();
		cv.put(COLUMN_CATEGORY, category);
		cv.put(COLUMN_CONTENT, content);
		// insert(String table, String nullColumnHack, ContentValues values)
		long code = getWritableDatabase().insert(TABLE_TODOLIST, null, cv);
		return code;
	}

	/**
	 * getTodoContentByCategory()
	 * 
	 * C*R*UD Retrieve
	 * 
	 * @param category
	 * 
	 * @returns a Cursor of *category* database records
	 */
	public Cursor getTodoContentByCategory(String category) {
		logIt("in gettodo " + category);

		String selection = COLUMN_CATEGORY + "=?";
		String[] selectionArgs = new String[] { category };
		// query(String table, String[] columns, String selection, String[] selectionArgs, String groupBy, String having, String orderBy)
		return getReadableDatabase().query(TABLE_TODOLIST, null, selection, selectionArgs, null, null, null);
	}

	/**
	 * getTodoContentForToday()
	 * 
	 * Retrieves one random todo element from the db.
	 * 
	 * @returns a random Cursor record
	 */
	public Cursor getTodoContentForToday() {
		String orderBy = "random()";
		String limit = "1";
		// query(String table, String[] columns, String selection, String[] selectionArgs, String groupBy, String having, String orderBy, String
		// limit)
		return getReadableDatabase().query(TABLE_TODOLIST, null, null, null, null, null, orderBy, limit);
	}

	/**
	 * getTodoContentForToday()
	 * 
	 * CR*U*D retrieve
	 * 
	 * @param id
	 * 
	 * @returns a Cursor record
	 */
	public Cursor getTodoContentById(long id) {
		String selection = COLUMN_ID + "=?";
		String[] selectionArgs = new String[] { String.valueOf(id) };
		// query(String table, String[] columns, String selection, String[] selectionArgs, String groupBy, String having, String orderBy)
		return getReadableDatabase().query(TABLE_TODOLIST, null, selection, selectionArgs, null, null, null);
	}

	/**
	 * updateTodo()
	 * 
	 * CR*U*D update
	 * 
	 * @param id
	 *            database id field
	 * @return number of rows affected
	 */
	public int updateTodo(Long id, String category, String content) {
		logIt("in delete" + id + " " + category + " " + content);
		ContentValues cv = new ContentValues();
		String[] args = { id.toString() };

		cv.put(COLUMN_CATEGORY, category);
		cv.put(COLUMN_CONTENT, content);

		// update(String table, ContentValues values, String whereClause,
		// String[] whereArgs)
		return getWritableDatabase().update(TABLE_TODOLIST, cv, COLUMN_ID + "=?", args);
	}

	/**
	 * deleteToDo()
	 * 
	 * CRU*D* Delete
	 * 
	 * @param id
	 *            database id field
	 * @return number of rows affected
	 */
	public int deleteToDo(int id) {
		logIt("in delete" + id);
		// delete(String table, String whereClause, String[] whereArgs)
		return getWritableDatabase().delete(TABLE_TODOLIST, COLUMN_ID + "=?", new String[] { String.valueOf(id) });
	}
	
	private void logIt(String msg) {
		final String TAG = "ass2";
		Log.d(TAG, msg);
	}
}
