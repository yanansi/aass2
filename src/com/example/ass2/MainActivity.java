package com.example.ass2;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import android.widget.AdapterView.OnItemClickListener;

/**
 * Main activity used to start the application. Displays a list of ToDo
 * categories.
 * 
 * @author Yiming Yan, Evgueni Freiman
 * @version 2015-11-02
 */
public class MainActivity extends Activity {
	public static final int SHOW_AS_ACTION_IF_ROOM = 1;
	public static final String CATEGORYKEY = "category";
	private ListView lv;
	private ArrayAdapter<String> aa = null;
	private String[] category;
	private SharedPreferences prefs;
	private long lastId;
	private Menu m;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		// The ui objects are now created, get a reference to the listview object
		lv = (ListView) findViewById(R.id.main_listview);
		// Use the strings.xml array
		category = getResources().getStringArray(R.array.category_array);
		aa = new ArrayAdapter<String>(this, R.layout.list_item, category);
		lv.setAdapter(aa);

		lv.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				Intent i = new Intent(view.getContext(), TodoListActivity.class);
				i.putExtra(CATEGORYKEY, category[position]);
				startActivity(i);
			}
		});
	}

	@Override
	public void onResume() {
		super.onResume();
		// Restore lastId from sharedPreferences
		prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
		lastId = prefs.getLong(DBHelper.COLUMN_ID, -1);
		
		// If lastId was set, make the "Last" menu option visible.
		if (m != null && lastId != -1) {
			m.getItem(1).setVisible(true);
		}
	}

	/**
	 * Adds our custom menu options to the menu
	 */
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		// Set up the intents for each button
		Intent today = new Intent(this, TodayTodoActivity.class);
		Intent last = new Intent(this, LastTodoActivity.class);
		logIt("main activity onCreateOptionsMenu: " + lastId);
		menu.add(R.string.today).setIntent(today).setShowAsAction(SHOW_AS_ACTION_IF_ROOM);
		menu.add(R.string.last).setIntent(last).setShowAsAction(SHOW_AS_ACTION_IF_ROOM);
		
		// If no saved SharedPreferences, the last menu item will be hidden 
		if (lastId == -1) {
			menu.getItem(1).setVisible(false);
		}
		// get the menu handle
		m = menu;

		return true;
	}

	private void logIt(String msg) {
		final String TAG = "ass2";
		Log.d(TAG, msg);
	}
}
