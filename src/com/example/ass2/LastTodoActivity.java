package com.example.ass2;

import android.app.Activity;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.TextView;

/**
 * Activity that displays the last viewed todo item
 * 
 * @author Yiming Yan, Evgueni Freiman
 * @version 2015-11-02
 */
public class LastTodoActivity extends Activity {
	private static DBHelper dbh;
	public static final int SHOW_AS_ACTION_IF_ROOM = 1;
	private long lastId;
	private SharedPreferences prefs;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.last);
		TextView category = (TextView) findViewById(R.id.last_textview1);
		TextView content = (TextView) findViewById(R.id.last_textview2);
		// instantiate the DBHelper class
		dbh = DBHelper.getDBHelper(this);
		prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
		// Get the last todo ID from sharedPrefs and then look it up in the db
		lastId = prefs.getLong(DBHelper.COLUMN_ID, -1);
		Cursor cursor = dbh.getTodoContentById(lastId);
		// If the ID does not exist, display a message saying it was deleted. Otherwise display the todo.
		if (cursor.getCount() == 0) {
			content.setText(R.string.tododeleted);
		} else {
			logIt(cursor.getCount() + "");
			cursor.moveToFirst();
			category.setText(cursor.getString(cursor.getColumnIndex(DBHelper.COLUMN_CATEGORY)));
			content.setText(cursor.getString(cursor.getColumnIndex(DBHelper.COLUMN_CONTENT)));
		}
	}
	
	private void logIt(String msg) {
		final String TAG = "ass2";
		Log.d(TAG, msg);
	}
}
